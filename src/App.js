import React, { Component } from 'react'
import AppDrawer from './components/containers/AppDrawer'
import { MuiThemeProvider, createMuiTheme, CircularProgress, withStyles } from '@material-ui/core'
import { API_TOKEN, CLIENT_ID, CLIENT_SECRET } from './config/api'
import axios from 'axios'

const theme = createMuiTheme({
  palette: {
    drawerWidth: 240 // sidebar width
  },
  Snackbar: {
    backgroundColor: '#fff'
  }
})

console.log(theme)

class App extends Component {
  state = {
    isAccessTokenSet: false,
    accessToken: null
  }
  
  componentDidMount () {
    axios
      .get(API_TOKEN, {
        params: {
          grant_type: 'password',
          client_id: CLIENT_ID,
          client_secret: CLIENT_SECRET,
          username: 'admin',
          password: 'qaz123wsx'
        }
      })
      .then((res) => {
        this.updateAccessToken(res.data)
      })
      .catch((error) => {      
        this.updateAccessToken(error)
      })
  }

  updateAccessToken = (res) => {
    this.setState({
      isAccessTokenSet: true,
      accessToken: res.access_token
    })
  }
  
  render () {
    const  { isAccessTokenSet, accessToken } = this.state
    const  { classes } = this.props
    
    return (
      <MuiThemeProvider theme={theme}>
        {
          isAccessTokenSet?
            <AppDrawer accessToken={accessToken} />
          :
            <div className={classes.circularProgressWrapper}>
              <CircularProgress className={classes.progress} />
            </div>
        }
      </MuiThemeProvider>
      )
  }
}

const styles = theme => ({
  circularProgressWrapper: {
    display: 'flex',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  progress: {
    margin: '50px'
  }
})


export default withStyles(styles, { withTheme: true })(App)
