import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import {
    ListItem,
    List,
    ListItemText,
    Slide,
    Paper,
    LinearProgress
} from '@material-ui/core'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import Dropzone from 'react-dropzone'
import axios from 'axios'
import { renderSize } from '../utils/functions'
import iconType from '../shared/iconType'
import { API_TOKEN, BASE_URL } from '../../config/api'
axios.defaults.withCredentials = true
class Uploader extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filesToUpload: [],
            openUploadWindow: false,
            isUploading: false,
            filesStatus: {}
        }
    }
    mergeToList(obj) {
        console.log(obj)
        this.props.onUploaded(obj)
    }
    getFileType(t) {
        return (t.startsWith('image')) ? 'image' : 'file'
    }
    handleUpload(obj, b64) {
        this.setState({ isUploading: true })
        this.state.filesStatus[obj.name] = 'uploading'
        // const url = 'http://www.mocky.io/v2/5bc967b9300000840085a4ef'
        const url = `${BASE_URL}/api/file`
        let params = {}
        params.name = obj.name
        params.directory = null
        params.type = this.getFileType(obj.type)
        params.size = renderSize(obj.size)
        params.file = b64
        params.blob = obj.preview
        const API_TOKEN = 'MDg2NDQ2YmVjMDlkYTRlZmY2NjY2MmZjYzE1YWM4MWIzYjJlZjc4OTcyODQ3ODU3MmNhMWM3ZWIzMzE1NjcyOA'
        axios({
            method: 'post',
            url: url,
            headers: {
                Authorization: 'Bearer ' + API_TOKEN
            },
            data: params
        })
        .then(res => {
            const response = res.data
            console.log(obj.name, response.success)
            this.mergeToList(response.data)
            this.setState({ isUploading: false })
            this.state.filesStatus[obj.name] = 'completed'
        })
        console.log('filesStatus: ', this.state.filesStatus)
    }
    onDrop(files) {
        console.log(files)
        const mergedFiles = [...this.state.filesToUpload, ...files]

        if (files.length <= 20) {
            files.map(f => {
                const reader = new FileReader()
                let b64
                reader.onload = (event) => {
                   b64 = event.target.result         // convert to base64
                   this.handleUpload(f, b64)
                }
                reader.readAsDataURL(f)
            })
            this.setState({
                filesToUpload: mergedFiles,
                openUploadWindow: true
            })
        }

    }

    render() {
      const {
        classes,
        children,
        dataCount,
        onUploaded
      } = this.props
      const { filesToUpload, openUploadWindow, isUploading } = this.state

      return (
        <React.Fragment>
          { /* listing */}
          <Dropzone
              onDrop={(files) => this.onDrop(files)}
              className={classes.uploader}
              disableClick={true}
          >
              {
                  dataCount > 0 ?
                      children
                      :
                      <div className={classes.center}>
                          <CloudUploadIcon className={classes.uploadIcon} />
                          <h3 className={classes.label}>Drop files here</h3>
                      </div>

              }
          </Dropzone>
          <Slide direction="up" in={openUploadWindow} mountOnEnter unmountOnExit>
                <Paper elevation={4} className={classes.paper}>
                    <List>
                        {filesToUpload.map(f => (
                            <ListItem key={f.name} className={classes.toUploadList}>
                                <div className={classes.flex}>
                                    { iconType(this.getFileType(f.type), classes.icon)  }
                                    <ListItemText className={classes.toUploadItem} classes={{ primary: classes.toUploadPri, secondary: classes.toUploadSec }} primary={f.name} secondary={renderSize(f.size)} />
                                </div>
                                { 
                                    isUploading === true ? 
                                        <LinearProgress color="secondary" className={classes.progress} /> 
                                        :
                                        ''
                                }
                            </ListItem>
                        ))}
                    </List>
                </Paper>
            </Slide>
        </React.Fragment>
    )
  }
}

const styles = theme => ({
    root: {

    },
    flex: {
        display: 'flex'
    },
    uploadIcon: {
        width: 80,
        height: 80,
        color: '#dadada',
        marginRight: 10
    },
    icon: {
        width: 20,
        height: 20,
        color: '#999'
    },
    paper: {
        width: 300,
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
        height: 300,
        overflowX: 'auto',
        padding: '10px 0 10px'
    },
    uploadButton: {
        float: 'right',
        marginTop: 10
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    uploader: {
        height: '100%',
        // overflowX: 'auto',
        // border: '1px dashed #aeaeae',
    },
    progress: {
        height: 1,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: '#eee'
    },
    label: {
        color: '#aaa',
        fontWeight: 'normal',
        margin: 0
    },
    center: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    },
    overflow: {
        overflowX: 'auto'
    },
    snackbar: {
        backgroundColor: '#fff',
    },
    toUploadList: {
        paddingTop: 0,
        paddingBottom: 5,
        paddingLeft: theme.spacing.unit * 2,
        display: 'block'
    },
    toUploadItem: {
        
    },
    toUploadPri: {
        fontSize: 13,
        width: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        lineHeight: 1,
        marginBottom: 4
    },
    toUploadSec: {
        fontSize: 10
    }
})

export default withStyles(styles, { withTheme: true })(Uploader)