import React from 'react'
import { 
  Dialog, 
  DialogActions, 
  DialogContent, 
  DialogContentText, 
  DialogTitle,
  Button,
  CircularProgress
} from '@material-ui/core'
import { Check } from '@material-ui/icons'

class ConfirmDialog extends React.Component {
  constructor (props) {
		super (props)

    this.state = {
      open: true
    }
  }

  onClose = () => {
    this.setState({ open: false })
  }

  render () {
    let {
      open = false,
      onCancel = null,
      onConfirm = null,
      isDeleting = false,
      children = null,
      message = 'This action cannot be undone.',
      title = 'Confirm Delete'
    } = this.props


    return (
      <Dialog scroll='body' open={open}>
        <DialogTitle>{title}</DialogTitle> 

        <DialogContent>
          { children }
          <DialogContentText>{ message }</DialogContentText>
        </DialogContent>

        <DialogActions>
          <Button color="primary" onClick={onCancel} disabled={isDeleting}>Cancel</Button>
          <Button color="primary" onClick={onConfirm} disabled={isDeleting}>
            Ok { isDeleting? <CircularProgress size={20} style={{marginLeft: '5px'}} /> : <Check /> }
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default ConfirmDialog
