import React from 'react'
import { 
  Dialog, 
  DialogActions, 
  DialogContent, 
  DialogTitle,
  Button,
  TextField
} from '@material-ui/core'

class NewFolderDialog extends React.Component {
  constructor (props) {
		super (props)

    this.state = {
      folderName: 'Untitled folder'
    }
  }

  handleChange = (event) => {
    this.setState({ folderName: event.target.value })
  }

  handleAdd = () => {
    const folderName = this.state.folderName
    this.props.onAdd(folderName)
  }

  render () {
    let {
      open = false,
      onClose
    } = this.props


    return (
      <Dialog 
        scroll='body' 
        open={open}
      >
        <DialogTitle>New Folder</DialogTitle> 

        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            value={this.state.folderName}
            onChange={this.handleChange}
            type="email"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleAdd} color="primary">
            Add
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default NewFolderDialog
