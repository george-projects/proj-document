import React from 'react'
import { 
  Dialog, 
  DialogActions, 
  DialogContent, 
  DialogTitle,
  Button,
  TextField
} from '@material-ui/core'

class RenameFolderDialog extends React.Component {
  constructor (props) {
		super (props)

    this.state = {
      name: ''
    }
  }

  handleChange = (event) => {
    this.setState({ name: event.target.value })
  }

  handleRename = () => {
    const newName = this.state.name.trim()
    const { data } = this.props
    
    if (newName) {
      this.props.onHandleRename(newName, data)
    }
  }

  componentWillUnmount () {
    console.log('componentWillUnmount')
  }

  render () {
    let { open, onClose, data } = this.props

    return (
      <Dialog 
        scroll='body' 
        open={open}
      >
        <DialogTitle>Rename</DialogTitle> 

        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            value={this.state.name}
            placeholder={data.name}
            onChange={this.handleChange}
            type="email"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleRename} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default RenameFolderDialog
