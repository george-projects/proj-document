import React from 'react'
import { 
  Paper,
  Avatar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Checkbox,
  withStyles
} from '@material-ui/core'
import iconType from './iconType'
import MoreOptions from './MoreOptions'
import RenameFolderDialog from '../dialog/RenameFolderDialog'
import { API_DIRECTORY } from '../../config/api'
import axios from 'axios'
import qs from 'qs'

class Listing extends React.Component {
  state = {
    isShowRenameDialog: false,
    itemClickedObj: {}
  }

  openRenameDialog = (index, obj) => {
    obj.index = index // assign index
    this.setState({
      isShowRenameDialog: true,
      itemClickedObj: obj
    })
  }

  onHandleRename = (newName, data) => {
    const { id, index } = data
    const { accessToken, onRenamed } = this.props
    const axiosData = {
      name: newName
    }
    const URL = `${API_DIRECTORY}/${id}`
    const axiosHeaders = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${accessToken}`
    }

    // close dialog
    this.setState({ isShowRenameDialog: false })

    onRenamed({
      "id": 6,
      "name": newName,
      "user": {
          "id": 3,
          "username": "admin",
          "email": "admin@example.com"
      },
      "child": [],
      "files": []
    }, index)
    
    // start axios update
    axios
      .put(URL, axiosData, {headers: axiosHeaders})
      .then((res) => {
        const { status, message, data } = res.data

        if (status) {
          onRenamed(data, index)
        } else {
          // TODO: Handle Error
        }
      })
      .catch((error) => {    
          // TODO: Handle Error
      })
  }

  rederDataRow = () => {
    const { classes, data, handleRowClick, handleOpenFolder, onViewMeta } = this.props

    return data.map((row, index) => { 
      let { type, id, size, lastModified, isSelected = false, user, name } = row
      type = type || 'folder'
      size = size || '-'
      const owner = user.username
      const icon = iconType(type || 'folder', classes.icon) 
      
      return (
        <TableRow 
          tabIndex={-1}
          selected={isSelected}
          hover 
          key={name}
        >
          <TableCell padding="none" style={{cursor: 'pointer'}} onClick={event => handleRowClick(event, index)}>
            <Checkbox color="primary" checked={isSelected} />
          </TableCell>
          <TableCell padding="none">
            <Avatar className={classes.avatar}>
              { icon  }
            </Avatar>
          </TableCell>
          <TableCell component="th" scope="row" padding="none" style={{cursor: 'pointer'}} onClick={event => handleOpenFolder(event, index)}>{ name }</TableCell>
          <TableCell>{ owner }</TableCell>
          <TableCell>{ lastModified }</TableCell>
          <TableCell>{ type }</TableCell>
          <TableCell>{ size }</TableCell>
          <TableCell>
            <MoreOptions openRenameDialog={this.openRenameDialog} index={index} data={row} onViewMeta={onViewMeta} />
          </TableCell>
        </TableRow>
      )
    })
  }

  render () {
    const { isShowRenameDialog, itemClickedObj } = this.state
    const { classes, data, handleSelectAllClick } = this.props
    const itemSelectedLength = data.filter((item, index) => data[index].isSelected === true).length
    const rowCount = data.length
    
    return (
      <React.Fragment>  
        <Paper className={classes.paper}>
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell padding="none">
                    <Checkbox
                      color="primary"
                      indeterminate={itemSelectedLength > 0 && itemSelectedLength < rowCount}
                      checked={itemSelectedLength === rowCount}
                      onChange={handleSelectAllClick}
                    />
                  </TableCell>
                  <TableCell></TableCell>
                  <TableCell padding="none">Name</TableCell>
                  <TableCell>Owner</TableCell>
                  <TableCell>Date Modified</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Size</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              { this.rederDataRow() }
              </TableBody>
            </Table>
          </div>
        </Paper>

        <RenameFolderDialog 
          open={isShowRenameDialog} 
          onClose={() => this.setState({ isShowRenameDialog: false})} 
          data={itemClickedObj}
          onHandleRename={this.onHandleRename} 
        />
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  paper: {
    width: '100%',
    marginBottom: '20px'
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  avatar: {
    width: 30,
    height: 30
  },
  icon: {
    fontSize: 18
  },
  emptyText: {
    margin: '20px 0'
  }
})

export default withStyles(styles)(Listing)