import React from 'react'
import { withStyles, Button  } from '@material-ui/core'
import { Home } from '@material-ui/icons'

const Breadcrumb = props => {
  const { 
    links, 
    classes,
    onClick 
  } = props

  const linkLength = links.length - 1
  
  const items =  links.map((item, index) => {
    let arrowRight = <i className={classes.arrowRight}></i>

    if (index === linkLength) {
      arrowRight = null
    }

    return (
      <li key={index} className={classes.li}>
        
        <Button color="primary" size='large' classes={{root: classes.button}} onClick={() => onClick(item.id)}>
        { index === 0? <Home/> : null } {item.label}
        </Button >

        {arrowRight}
      </li> 
    )
  })

  return (
    <React.Fragment>
      <ul className={classes.ul}>
        { items }      
      </ul>
    </React.Fragment>
  )
}

const styles = theme => ({
  ul: {
    listStyle: 'none',
    margin: 0,
    padding: 0
  },
  li: {
    display: 'inline-block',
    margin: '0'
  },
  arrowRight: {
    border: `solid ${theme.palette.primary.main}`,
    borderWidth: '0 2px 2px 0',
    display: 'inline-block',
    padding: '4px',
    transform: 'rotate(-45deg)',
    margin: '0 10px'
  },
  button: {
    padding: '10px 6px',
    width: 'auto',
    minWidth: 0
  }
})

export default withStyles(styles)(Breadcrumb)