import React from 'react'
import { withStyles, Button, Typography } from '@material-ui/core'
import { Delete, CloudDownload } from '@material-ui/icons'
import AddButton from './AddButton'

const ButtonToolBar = props => {
  const { 
    classes, 
    itemSelectedLength,
    onNewFolder,
    onFileUpload,
    onDelete,
    onDownload,
    onViewInfo
  } = props

  return (
    <div className={classes.wrapper}>
      <AddButton onNewFolder={onNewFolder} onFileUpload={onFileUpload} />

      <div>
        <div className={classes.label}>
          <Typography color="inherit" variant="subtitle1">
            { itemSelectedLength } selected
          </Typography>
        </div>

        <Button variant="fab" mini color='primary' aria-label="Delete" className={classes.button} disabled={!itemSelectedLength} onClick={onDelete}>
          <Delete />
        </Button>

        <Button variant="fab" mini color='primary' aria-label="Download" className={classes.button} disabled={!itemSelectedLength} onClick={onDownload}>
          <CloudDownload />
        </Button>
      </div>
    </div>
  )
}

const styles = theme => ({  
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  label: {
    height: '40px',
    display: 'inline-block',
    marginRight: '15px'
  },
  button: {
    margin: theme.spacing.unit
  }
})

export default withStyles(styles)(ButtonToolBar)