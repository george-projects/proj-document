import React from 'react'
import { withStyles, IconButton, Menu, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Edit, Info, MoreVert } from '@material-ui/icons'

class MoreOptions extends React.Component {
  state = {
    anchorEl: null,
  }

  handleMore = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  onRename = () => {
    const { index, data, openRenameDialog} = this.props
    openRenameDialog(index, data)
    this.handleClose()
  }

  onViewMeta = () => {
    const { index, data, onViewMeta} = this.props
    onViewMeta(index, data)
    this.handleClose()
  }

  handleClose = () => {    
    this.setState({ anchorEl: null })
  }

  render() {
    const { anchorEl } = this.state
    const { classes } = this.props
    
    return (
      <div className={classes.addButton}>
        <IconButton 
          onClick={this.handleMore}
        >
          <MoreVert />
        </IconButton>
        <Menu
          id="menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.onRename}>
            <ListItemIcon>
              <Edit />
            </ListItemIcon>
            <ListItemText inset primary="Rename" />
          </MenuItem>
          <MenuItem onClick={this.onViewMeta}>
            <ListItemIcon>
              <Info />
            </ListItemIcon>
            <ListItemText inset primary="View Meta" />
          </MenuItem>
        </Menu>
      </div>
    )
  }
}

const styles = theme => ({
  addButton: {
    margin: theme.spacing.unit,
    display: 'inline-block'
  }
})

export default withStyles(styles)(MoreOptions)