import React from 'react'
import { withStyles, Button, Menu, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Add, CreateNewFolder, CloudUpload } from '@material-ui/icons'

class AddButton extends React.Component {
  state = {
    anchorEl: null,
  }

  handleAdd = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleFolder = () => {
    this.handleClose()
    this.props.onNewFolder()
  }

  handleFileUpload = () => {
    this.handleClose()
    this.props.onFileUpload()
  }

  handleClose = () => {    
    this.setState({ anchorEl: null })
  }

  render() {
    const { anchorEl } = this.state
    const { classes } = this.props
    
    return (
      <div className={classes.addButton}>
        <Button 
          variant="fab" 
          mini
          color='primary' 
          aria-label='Add'          
          aria-owns={anchorEl ? 'menu' : null}
          aria-haspopup='true'
          onClick={this.handleAdd}
        >
          <Add />
        </Button>
        <Menu
          id="menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleFolder}>
            <ListItemIcon>
              <CreateNewFolder />
            </ListItemIcon>
            <ListItemText inset primary="Folder" />
          </MenuItem>
          <MenuItem onClick={this.handleFileUpload}>
            <ListItemIcon>
              <CloudUpload />
            </ListItemIcon>
            <ListItemText inset primary="File upload" />
          </MenuItem>
        </Menu>
      </div>
    )
  }
}

const styles = theme => ({
  addButton: {
    margin: theme.spacing.unit,
    display: 'inline-block'
  }
})

export default withStyles(styles)(AddButton)