import React from 'react'
import { Folder, InsertDriveFile, InsertPhoto  } from '@material-ui/icons'

export default function iconType (type, className) {
  let icon = <InsertDriveFile className={className} />

  switch (type) {
    case 'folder': 
      icon = <Folder className={className} />
    break

    case 'file':
      icon = <InsertDriveFile className={className} />
    break

    case 'image':
      icon = <InsertPhoto className={className} />
    break
  }

  return icon
}