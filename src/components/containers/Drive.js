import React from 'react'
import { withStyles, Divider, Grid, CircularProgress } from '@material-ui/core'
import Breadcrumb from '../shared/Breadcrumb'
import Listing from '../shared/Listing'
import NewFolderDialog from '../dialog/NewFolderDialog'
import ConfirmDialog from '../dialog/ConfirmDialog'
import ButtonToolBar from '../shared/ButtonToolBar'
import MetaData from '../containers/MetaData'
import Uploader from '../dropzone/Uploader'
import { API_DIRECTORY, BASE_URL } from '../../config/api'
import axios from 'axios'
import qs from 'qs'


class Drive extends React.Component {
  constructor (props) {
		super (props)

    this.isUnMounted = false

    this.state = {
      isShowDeleteDialog: false,
      isShowNewFolderDialog: false,
      isShowMetaData: false,
      isFetchingData: true,
      data: [],
      breadcrumb: [],
    }
  }

  componentWillUnmount () {
    this.isUnMounted = true
  }

  componentDidMount () {
    this.openDirectory(0)
  }

  openDirectory = (directoryID) => {
    const { accessToken } = this.props
    const url = directoryID? `${API_DIRECTORY}/${directoryID}` : API_DIRECTORY
    
    this.setState({ isFetchingData: true })
    
    axios
      .get(url, {
        params: {
          access_token: accessToken
        }
      })
      .then((res) => {
        if (res.data.status) {
          let data = res.data.data

          // Add variant
          data.child.map(item => item.variant = 'folder')
          data.files.map(item => item.variant = 'file')

          // Merge 2 arrays of object
          data = this.mergeArray(data.child, data.files)
          this.updateData(data)
        } else {
          // TODO: Handle Error
        }
      })
      .catch((error) => {     
        // TODO: Handle Error     
        this.updateData([])
      })
  }

  mergeArray = (arr1, arr2) => [...arr1, ...arr2]

  updateData (data) {
    if (this.isUnMounted) return // don't continue if the component is already unmounted
    console.log('updateData: ', data)

    // Update breadcrumb
    let breadcrumb  = [...this.state.breadcrumb] // make immutable
    breadcrumb = [
      {label: 'Root', id: 0}
    ]


    this.setState({ isFetchingData: false, data, breadcrumb }) 
  }
  
  handleNewFolderDialog = (folderName) => {
    const { accessToken } = this.props
    const data = [...this.state.data] // make immutable

    const newFolderData = {
      "id": 2,
      "variant": "folder",
      "name": folderName,
      "user": {
        "id": 3,
        "username": "admin",
        "email": "admin@example.com"
      }
    }

    // const axiosData = {
    //   name: folderName,
    //   parent: null
    // }

    // const axiosHeaders = {
    //   'Content-Type': 'application/json',
    //   'Authorization': `Bearer ${accessToken}`
    // }
    // axios
    //   .post(API_DIRECTORY, qs.stringify(axiosData), {headers: axiosHeaders})
    //   .then((res) => {
    //     console.log(res.data)
    //   })
    //   .catch((error) => {    
    //   })
    

    // Merge Data
    data.unshift(newFolderData)
    this.setState({ isShowNewFolderDialog: false, data })
  }

  handleDeleteDialog = () => {
    if (this.isUnMounted) return // don't continue if the component is already unmounted
    const data = [...this.state.data] // make immutable
    let toDeleteID = []
    toDeleteID.push(data.filter((item) => item.isSelected === true).map(item => item.id))
    console.log(toDeleteID)
    const url = `${BASE_URL}/api/file`
    axios.delete(url, { data: { ids: toDeleteID } })
    .then((res) => {
      console.log(res)     
    })
    const newData = data.filter((item, index) => !data[index].isSelected)
    this.setState({ isShowDeleteDialog: false, data: newData })
  }

  handleFileUpload = () => {
    alert('handleFileUpload')
  }

  handleDelete = () => {
    this.setState({ isShowDeleteDialog: true })
  }

  handleDownload = () => {
    console.log('onDownload')
  }

  handleViewInfo = (index, obj) => {
    const isShowMetaData = this.state.isShowMetaData
    this.setState({
      isShowMetaData: !isShowMetaData
    })
  }

  onRenamed = (responseData, index) => {
    let data  = [...this.state.data] // make immutable
    
    // update data row via index
    data[index] = responseData

    // update the base data
    this.setState({ data })
  }

  handleListingRowClick = (event, index) => {
    let data = [...this.state.data]

    if (data[index] && data[index].isSelected) {
      data[index].isSelected = false
    } else {
      data[index].isSelected = true
    }

    this.setState({ data })
  }

  handleOpenFolder = (event, index) => {
    let data = [...this.state.data]
    const variant = data[index].variant
    const id = data[index].id

    // check if clicked item is a folder
    if (variant !== 'folder') return 

    // then open it
    this.openDirectory(id)
  }

  handleCheckBoxAllClick = (event) => {
    let data  = [...this.state.data] // make immutable

    if (event.target.checked) {
      data.map((item, index) => data[index].isSelected = true)     
    } else {
      data.map((item, index) => data[index].isSelected = false)    
    }
    
    this.setState({ data })
  }
  
  handleUploadedFile = (uploadedFile) => {
    console.log('uploadedFile', uploadedFile)
  }

  hanldeBreadcrumbClick = (directoryID) => {
    // open directory
    this.openDirectory(directoryID)
  }

  render () {
    const { 
      isShowNewFolderDialog,
      isShowDeleteDialog, 
      isShowMetaData,
      data,
      isFetchingData,
      breadcrumb
    } = this.state
    const { accessToken } = this.props
    const itemSelectedLength = data.filter((item, index) => data[index].isSelected === true).length
    const { classes } = this.props
    let metaData = null

    if (isShowMetaData) {
      metaData = <Grid item xs={3}><MetaData /></Grid>
    }

    return (
      <Grid container style={{height: '100%'}} spacing={24}>
        <Grid item xs={isShowMetaData? 9 : 12 }>
          <ButtonToolBar 
            onNewFolder={() => this.setState({ isShowNewFolderDialog: true })} 
            onFileUpload={this.handleFileUpload} 
            onDelete={this.handleDelete}
            onDownload={this.handleDownload}
            itemSelectedLength={itemSelectedLength} 
          />
          <Divider inset classes={{root: classes.devider}}/>
          <Breadcrumb 
            links={breadcrumb} 
            onClick={this.hanldeBreadcrumbClick}
          />
          {
            isFetchingData?
              <div className={classes.circularProgressWrapper}>
                <CircularProgress className={classes.progress} />
              </div>
            :
              <React.Fragment>
                <Uploader dataCount={data.length} onUploaded={this.handleUploadedFile}>
                  <Listing 
                    data={data} 
                    handleRowClick={this.handleListingRowClick} 
                    handleSelectAllClick={this.handleCheckBoxAllClick} 
                    handleOpenFolder={this.handleOpenFolder}
                    onRenamed={this.onRenamed}
                    onViewMeta={this.handleViewInfo}
                    accessToken={accessToken}
                  />
                </Uploader>
                <ConfirmDialog open={isShowDeleteDialog} onCancel={() => this.setState({ isShowDeleteDialog: false })} onConfirm={this.handleDeleteDialog} />
              </React.Fragment>
          }
          <NewFolderDialog 
            open={isShowNewFolderDialog} 
            onClose={() => this.setState({ isShowNewFolderDialog: false})} 
            onAdd={this.handleNewFolderDialog}
          />
        </Grid>
        { metaData }
      </Grid>
    )
  }
}

const styles = theme => ({
  circularProgressWrapper: {
    display: 'flex',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  devider: {
    margin: '10px 0 0 0'
  },
  progress: {
    margin: '50px'
  }
})

export default withStyles(styles, { withTheme: true })(Drive)