import React from 'react'
import { withStyles, Hidden, Drawer } from '@material-ui/core'
import SidebarMenu from './SidebarMenu'

const Sidebar = props => {
    const { classes, open, onClose } = props

    return (
      <React.Fragment>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            anchor='left'
            open={open}
            onClose={onClose}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <SidebarMenu /> 
          </Drawer>
        </Hidden>

        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <SidebarMenu /> 
          </Drawer>
        </Hidden>
        
      </React.Fragment>
    )
}

const styles = theme => ({
  drawerPaper: {
    width: theme.palette.drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    }
  }
})

export default withStyles(styles, { withTheme: true })(Sidebar)