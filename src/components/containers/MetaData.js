import React from 'react'
import { withStyles } from '@material-ui/core'

const MetaData = props => {
    const { classes } = props

    return (
      <React.Fragment>
        Meta
      </React.Fragment>
    )
}

const styles = theme => ({
})

export default withStyles(styles, { withTheme: true })(MetaData)