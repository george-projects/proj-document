import React from 'react'
import { withStyles  } from '@material-ui/core'
import Header from './Header'
import Sidebar from './Sidebar'
import Drive from './Drive'


class AppDrawer extends React.Component {
  state = {
    mobileOpen: false
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }))
  }

  render() {
    const { classes, accessToken } = this.props
    const { mobileOpen } = this.state

    return (
      <div className={classes.root}>        
        <Header handleDrawerToggle={this.handleDrawerToggle}/>
        <Sidebar open={mobileOpen} onClose={this.handleDrawerToggle} />
        
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Drive accessToken={accessToken} />
        </main>
      </div>
    )
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: `100%`,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    position: 'relative'
  }
})

export default withStyles(styles, { withTheme: true })(AppDrawer)