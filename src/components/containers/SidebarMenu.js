import React from 'react'
import { withStyles, Divider, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import InboxIcon from '@material-ui/icons/MoveToInbox'

const SidebarMenu = props => {
    const { classes } = props

    return (
      <React.Fragment>
        <div className={classes.toolbar} />
        <Divider />
        <List>
          <ListItem button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="My Drive" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
        </List>
      </React.Fragment>
    )
}

const styles = theme => ({
  toolbar: theme.mixins.toolbar
})

export default withStyles(styles, { withTheme: true })(SidebarMenu)