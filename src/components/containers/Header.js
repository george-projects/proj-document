import React from 'react'
import { withStyles, AppBar, Toolbar, IconButton, Typography } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'

const Header = props => {
    const { classes, handleDrawerToggle } = props

    return (
      <React.Fragment>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" aria-label="Open drawer" onClick={handleDrawerToggle} className={classes.navIconHide}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>Document Management</Typography>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    )
}

const styles = theme => ({
  appBar: {
    position: 'absolute',
    marginLeft: theme.palette.drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${theme.palette.drawerWidth}px)`,
    }
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    }
  }
})

export default withStyles(styles, { withTheme: true })(Header)