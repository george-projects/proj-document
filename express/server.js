var express = require('express')
var proxy = require('express-http-proxy');
var setup = require('./setup.js');
var app = express()

// global controller
app.all('/*',function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin', setup.devServer);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    // res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// A friendly hello :)
app.all('/hello', function (req, res) {
  console.log('Saying Hello');
  res.send('Hello World');
})

// run all proxys through here (setup.proxyHost)
// Example:
// http://localhost:8082/proxy/searchautocomplete/ajax/get/?q=goodyear
app.use('/proxy', proxy(setup.proxyHost, {
  proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
    console.log('Running proxys from ' + setup.proxyHost);
    return proxyReqOpts;
  }
}));

// app.use('/cart', require('./requests/getCompatible.js'));

app.listen(setup.port, function () {
  console.log('Running server on port '+ setup.port)
})
