var express = require('express');
var app = express.Router();

var data = {}

data.compatible = {
  "compatible": [
    "530714",
    "MY_50019",
    // "521150",
    "0360258"
    // "541761-P-ONSALE-205/80R16"
  ]
}

app.route('/getcompatible').all(function(req,res){
  console.log('get compatible');
  res.send(data.compatible);
});

module.exports = app;
