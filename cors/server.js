var express = require('express')
var cors = require('cors')
var app = express()

var port = 5555

app.use(cors())

// app.get('/products/:id', function (req, res, next) {
//   res.json({msg: 'This is CORS-enabled for all origins!'})
// })

var corsOptions = {
  origin: 'https://dev-documents.tgo-asia.com/app_dev.php',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.listen(port, function () {
  console.log('CORS-enabled web server listening on port: ' + port)
})